# -*- coding: utf-8 -*-
import re
from south.db import db
from south.v2 import SchemaMigration
from django.utils.encoding import smart_str, force_unicode
from django.utils.safestring import mark_safe
from docutils.core import publish_parts


TAG_REGEX = re.compile(r'<[^>]+>')


class Migration(SchemaMigration):
    def restructuredtext(self, value):
        parts = publish_parts(source=smart_str(value), writer_name="html4css1")
        return mark_safe(force_unicode(parts["fragment"]))

    def forwards(self, orm):
        # Adding field 'Content.summary_raw'
        db.add_column('publisher_content', 'summary_raw',
                      self.gf('django.db.models.fields.TextField')(default=u''),
                      keep_default=False)

        # Adding field 'Content.content_raw'
        db.add_column('publisher_content', 'content_raw',
                      self.gf('django.db.models.fields.TextField')(default=u''),
                      keep_default=False)

        # Adding field 'Content.summary_html'
        db.add_column('publisher_content', 'summary_html',
                      self.gf('django.db.models.fields.TextField')(default=u''),
                      keep_default=False)

        # Adding field 'Content.content_html'
        db.add_column('publisher_content', 'content_html',
                      self.gf('django.db.models.fields.TextField')(default=u''),
                      keep_default=False)

        # Adding field 'Content.summary_notags'
        db.add_column('publisher_content', 'summary_notags',
                      self.gf('django.db.models.fields.TextField')(default=u''),
                      keep_default=False)

        # Adding field 'Content.content_notags'
        db.add_column('publisher_content', 'content_notags',
                      self.gf('django.db.models.fields.TextField')(default=u''),
                      keep_default=False)

        # Adding field 'PrivateNote.summary_raw'
        db.add_column('publisher_privatenote', 'summary_raw',
                      self.gf('django.db.models.fields.TextField')(default=u''),
                      keep_default=False)

        # Adding field 'PrivateNote.content_raw'
        db.add_column('publisher_privatenote', 'content_raw',
                      self.gf('django.db.models.fields.TextField')(default=u''),
                      keep_default=False)

        # Adding field 'PrivateNote.summary_html'
        db.add_column('publisher_privatenote', 'summary_html',
                      self.gf('django.db.models.fields.TextField')(default=u''),
                      keep_default=False)

        # Adding field 'PrivateNote.content_html'
        db.add_column('publisher_privatenote', 'content_html',
                      self.gf('django.db.models.fields.TextField')(default=u''),
                      keep_default=False)

        # Adding field 'PrivateNote.summary_notags'
        db.add_column('publisher_privatenote', 'summary_notags',
                      self.gf('django.db.models.fields.TextField')(default=u''),
                      keep_default=False)

        # Adding field 'PrivateNote.content_notags'
        db.add_column('publisher_privatenote', 'content_notags',
                      self.gf('django.db.models.fields.TextField')(default=u''),
                      keep_default=False)

        if not db.dry_run:
            contents = orm['publisher.content'].objects.all()
            privatenotes = orm['publisher.privatenote'].objects.all()
            for model in (contents, privatenotes):
                for c in model:
                    c.summary_raw = c.summary
                    c.content_raw = c.content
                    c.summary_html = self.restructuredtext(c.summary_raw)
                    c.summary_notags = TAG_REGEX.sub('', c.summary_html)
                    c.content_html = self.restructuredtext(c.content_raw)
                    c.content_notags = TAG_REGEX.sub('', c.content_html)
                    c.save()

    def backwards(self, orm):
        # Deleting field 'Content.summary_raw'
        db.delete_column('publisher_content', 'summary_raw')

        # Deleting field 'Content.content_raw'
        db.delete_column('publisher_content', 'content_raw')

        # Deleting field 'Content.summary_html'
        db.delete_column('publisher_content', 'summary_html')

        # Deleting field 'Content.content_html'
        db.delete_column('publisher_content', 'content_html')

        # Deleting field 'Content.summary_notags'
        db.delete_column('publisher_content', 'summary_notags')

        # Deleting field 'Content.content_notags'
        db.delete_column('publisher_content', 'content_notags')

        # Deleting field 'PrivateNote.summary_raw'
        db.delete_column('publisher_privatenote', 'summary_raw')

        # Deleting field 'PrivateNote.content_raw'
        db.delete_column('publisher_privatenote', 'content_raw')

        # Deleting field 'PrivateNote.summary_html'
        db.delete_column('publisher_privatenote', 'summary_html')

        # Deleting field 'PrivateNote.content_html'
        db.delete_column('publisher_privatenote', 'content_html')

        # Deleting field 'PrivateNote.summary_notags'
        db.delete_column('publisher_privatenote', 'summary_notags')

        # Deleting field 'PrivateNote.content_notags'
        db.delete_column('publisher_privatenote', 'content_notags')


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'publisher.content': {
            'Meta': {'ordering': "(u'-entry___sort_date', u'language')", 'unique_together': "((u'entry', u'language'),)", 'object_name': 'Content'},
            '_default_tags': (u'lck.django.tags.models.DefaultTags', [], {'unique': 'False', 'primary_key': 'False', 'db_column': 'None', 'blank': 'True', u'default': "u''", 'null': 'False', 'db_index': 'False'}),
            'cache_version': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'content': ('django.db.models.fields.TextField', [], {}),
            'content_html': ('django.db.models.fields.TextField', [], {'default': "u''"}),
            'content_notags': ('django.db.models.fields.TextField', [], {'default': "u''"}),
            'content_raw': ('django.db.models.fields.TextField', [], {'default': "u''"}),
            'created': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'created_by': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'+'", 'on_delete': 'models.SET_NULL', 'default': 'None', 'to': "orm['publisher.Profile']", 'blank': 'True', 'null': 'True'}),
            'display_count': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'entry': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['publisher.Entry']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'language': ('django.db.models.fields.PositiveIntegerField', [], {'default': '40'}),
            'modified': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'modified_by': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'+'", 'on_delete': 'models.SET_NULL', 'default': 'None', 'to': "orm['publisher.Profile']", 'blank': 'True', 'null': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'summary': ('django.db.models.fields.TextField', [], {}),
            'summary_html': ('django.db.models.fields.TextField', [], {'default': "u''"}),
            'summary_notags': ('django.db.models.fields.TextField', [], {'default': "u''"}),
            'summary_raw': ('django.db.models.fields.TextField', [], {'default': "u''"}),
            'title': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100', 'db_index': 'True'})
        },
        'publisher.entry': {
            'Meta': {'ordering': "(u'-_sort_date',)", 'object_name': 'Entry'},
            '_sort_date': ('django.db.models.fields.DateTimeField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'cache_version': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'comment_status': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'created_by': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'+'", 'on_delete': 'models.SET_NULL', 'default': 'None', 'to': "orm['publisher.Profile']", 'blank': 'True', 'null': 'True'}),
            'display_count': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'include_on_frontpage': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'modified': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'modified_by': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'+'", 'on_delete': 'models.SET_NULL', 'default': 'None', 'to': "orm['publisher.Profile']", 'blank': 'True', 'null': 'True'}),
            'publication_status': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'source_name': ('django.db.models.fields.CharField', [], {'default': "u''", 'max_length': '100', 'blank': 'True'}),
            'source_url': ('django.db.models.fields.URLField', [], {'default': 'None', 'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'visible_from': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'visible_to': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        'publisher.privateauthlog': {
            'Meta': {'ordering': "(u'-modified',)", 'object_name': 'PrivateAuthLog'},
            'address': ('django.db.models.fields.IPAddressField', [], {'max_length': '15'}),
            'cache_version': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'failed_key': ('django.db.models.fields.CharField', [], {'default': "u''", 'max_length': '32', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'note': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['publisher.PrivateNote']"}),
            'success': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'publisher.privatenote': {
            'Meta': {'ordering': "(u'-_sort_date',)", 'object_name': 'PrivateNote'},
            '_sort_date': ('django.db.models.fields.DateTimeField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'cache_version': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'comment_status': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'content': ('django.db.models.fields.TextField', [], {}),
            'content_html': ('django.db.models.fields.TextField', [], {'default': "u''"}),
            'content_notags': ('django.db.models.fields.TextField', [], {'default': "u''"}),
            'content_raw': ('django.db.models.fields.TextField', [], {'default': "u''"}),
            'created': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'created_by': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'+'", 'on_delete': 'models.SET_NULL', 'default': 'None', 'to': "orm['publisher.Profile']", 'blank': 'True', 'null': 'True'}),
            'display_count': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'include_on_frontpage': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'language': ('django.db.models.fields.PositiveIntegerField', [], {'default': '40'}),
            'modified': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'modified_by': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'+'", 'on_delete': 'models.SET_NULL', 'default': 'None', 'to': "orm['publisher.Profile']", 'blank': 'True', 'null': 'True'}),
            'private_key': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'publication_status': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'summary': ('django.db.models.fields.TextField', [], {}),
            'summary_html': ('django.db.models.fields.TextField', [], {'default': "u''"}),
            'summary_notags': ('django.db.models.fields.TextField', [], {'default': "u''"}),
            'summary_raw': ('django.db.models.fields.TextField', [], {'default': "u''"}),
            'title': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100', 'db_index': 'True'}),
            'visible_from': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'visible_to': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        'publisher.profile': {
            'Meta': {'object_name': 'Profile'},
            'birth_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'country': ('django.db.models.fields.PositiveIntegerField', [], {'default': '153'}),
            'gender': ('django.db.models.fields.PositiveIntegerField', [], {'default': '2'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_active': ('django.db.models.fields.DateTimeField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'nick': ('django.db.models.fields.CharField', [], {'default': "u''", 'max_length': '30', 'blank': 'True'}),
            'time_zone': ('django.db.models.fields.FloatField', [], {'default': '1.0'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['auth.User']", 'unique': 'True'})
        },
        'tags.tag': {
            'Meta': {'object_name': 'Tag'},
            'author': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['publisher.Profile']"}),
            'cache_version': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'tags_tag_tags'", 'to': "orm['contenttypes.ContentType']"}),
            'created': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'language': ('django.db.models.fields.PositiveIntegerField', [], {'default': '40'}),
            'modified': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '75'}),
            'object_id': ('django.db.models.fields.IntegerField', [], {'db_index': 'True'}),
            'official': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'stem': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'related_tags'", 'null': 'True', 'to': "orm['tags.TagStem']"})
        },
        'tags.tagstem': {
            'Meta': {'object_name': 'TagStem'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'language': ('django.db.models.fields.PositiveIntegerField', [], {'default': '40'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '75'}),
            'tag_count': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'})
        }
    }

    complete_apps = ['publisher']
