#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (C) 2012 by Łukasz Langa
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from collections import defaultdict

from django.conf.urls.defaults import *
from django.contrib.sitemaps import Sitemap
from django.contrib.sites.models import Site, get_current_site
from django.contrib.syndication.views import Feed
from django.shortcuts import get_object_or_404
from dj.choices import Language
from lck.django.tags.models import TagStem

from langapl.publisher.models import Content


class FlatPageSitemap(Sitemap):
    def items(self):
        current_site = Site.objects.get_current()
        return current_site.flatpage_set.filter(registration_required=False)

    def location(self, obj):
        return "{}?lang={}".format(obj.get_absolute_url(),
            Language.name_from_id(obj.language))

    def lastmod(self, obj):
        return obj.modified


class PublisherSitemap(Sitemap):
    priority = 0.6

    def items(self):
        return Content.objects.all()

    def lastmod(self, obj):
        return obj.modified


sitemaps = {
    'flatpages': FlatPageSitemap,
    'langapl': PublisherSitemap,
}


class RecentEntriesFeed(Feed):
    author_email = 'lukasz@langa.pl'
    author_name = 'Łukasz Langa'

    def get_object(self, request, lang_code):
        return lang_code

    def title(self, obj):
        msg = defaultdict(lambda: "lukasz.langa.pl: Recent Entries",
            {Language.pl.name: "lukasz.langa.pl: Najnowsze wpisy"})
        return msg[obj]

    def description(self, obj):
        msg = defaultdict(lambda: "Latest entries published on the website.",
            {Language.pl.name: "Ostatnie artykuły opublikowane w serwisie."})
        return msg[obj]

    def link(self, obj):
        return '/'

    def items(self, obj):
        lang = Language.id_from_name(obj)
        return Content.objects.filter(language=lang,
            entry__include_on_frontpage=True).select_related()[:10]

    def item_title(self, item):
        return item.title

    def item_description(self, item):
        return item.summary_notags

    def item_pubdate(self, item):
        return item.entry._sort_date

    def feed_extra_kwargs(self, obj):
        return {'obj': obj}

    def feed_type(self, **kwargs):
        kwargs['language'] = kwargs['obj'].decode()
        return Feed.feed_type(**kwargs)


class TagFeed(Feed):
    author_email = 'lukasz@langa.pl'
    author_name = 'Łukasz Langa'

    def get_object(self, request, tag_stem, lang_code):
        stem = get_object_or_404(TagStem, name=tag_stem,
                language=Language.id_from_name(lang_code))
        return stem, lang_code

    def title(self, obj):
        tag_stem, lang_code = obj
        msg = defaultdict(lambda: "lukasz.langa.pl: Recent entries tagged as "
                "{}",
            {Language.pl.name: "lukasz.langa.pl: Najnowsze wpisy otagowane "
                "jako {}"})
        return msg[lang_code].format(tag_stem.name)

    def description(self, obj):
        tag_stem, lang_code = obj
        msg = defaultdict(lambda: "Latest entries published on the website "
                "with the tag \"{}\".",
            {Language.pl.name: "Ostatnie artykuły opublikowane w serwisie "
                "z tagiem \"{}\"."})
        return msg[lang_code].format(tag_stem.name)

    def link(self, obj):
        return '/'

    def items(self, obj):
        tag_stem, lang_code = obj
        return TagStem.objects.get_content_objects(model=Content,
            stem=tag_stem, order_by=('-modified',))

    def item_title(self, item):
        return item.title

    def item_description(self, item):
        return item.summary_notags

    def item_pubdate(self, item):
        return item.entry._sort_date

    def feed_extra_kwargs(self, obj):
        tag_stem, lang_code = obj
        return {'tag_stem': tag_stem, 'lang_code': lang_code}

    def feed_type(self, **kwargs):
        kwargs['language'] = kwargs['lang_code'].decode()
        return Feed.feed_type(**kwargs)


urlpatterns = patterns('',
    url(r'^$', 'langapl.publisher.views.recent'),
    url(r'^login/$', 'langapl.publisher.views.login'),
    url(r'^logout/$', 'langapl.publisher.views.logout'),
    url(r'^tag/$', 'langapl.publisher.views.tagcloud', name='tagcloud'),
    url(r'^tag/([^/]+)/$', 'langapl.publisher.views.tag', name='tag'),
    url(r'^arch/$', 'langapl.publisher.views.archive', name='archive'),
    url(r'^sitemap\.xml$', 'django.contrib.sitemaps.views.sitemap',
        kwargs={'sitemaps': sitemaps}),
    url(r'^feed/recent/rss-(?P<lang_code>.+)\.xml$', RecentEntriesFeed()),
    url(r'^feed/tag/(?P<tag_stem>[^/]+)/rss-(?P<lang_code>.+)\.xml$', TagFeed()),
    url(r'^p/(?P<slug>.+)/$', 'langapl.publisher.views.private', name='private'),
    url(r'^(?P<entry_id>\d+)/$', 'langapl.publisher.views.show',
        name='show'),
    url(r'^(?P<entry_id>\d+)/(?P<slug>.+)/$', 'langapl.publisher.views.show',
        name='show'),
)
