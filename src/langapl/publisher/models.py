#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (C) 2012 by Łukasz Langa
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from datetime import datetime
import re

from docutils.core import publish_parts
from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.sitemaps import ping_google
from django.core.mail import mail_admins
from django.db import models as db
from django.dispatch import receiver
from django.utils.encoding import smart_str, force_unicode
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _
from lck.django import cache
from lck.django.activitylog.models import MonitoredActivity
from lck.django.choices import Choices, Language
from lck.django.common.models import (Titled, Slugged, TimeTrackable,
     EditorTrackable, DisplayCounter, Localized)
from lck.django.profile.models import BasicInfo, GravatarSupport
from lck.django.tags.models import Taggable


TAG_REGEX = re.compile(r'<[^>]+>')
RESTRUCTUREDTEXT_FILTER_SETTINGS = getattr(
        settings, "RESTRUCTUREDTEXT_FILTER_SETTINGS", {},
)


class Profile(BasicInfo, GravatarSupport, MonitoredActivity):
    class Meta:
        verbose_name = _("profile")
        verbose_name_plural = _("profiles")

    def __unicode__(self):
        return self.nick


class PublicationStatus(Choices):
    _ = Choices.Choice

    draft = _("Draft")
    needs_edit = _("Needs Edit")
    published = _("Published")

    @classmethod
    @Choices.ToIDs
    def viewable(cls):
        return (cls.published,)


class CommentStatus(Choices):
    _ = Choices.Choice

    open = _("Open")
    read_only = _("Read-only")
    closed = _("Closed")

    @classmethod
    @Choices.ToIDs
    def viewable(cls):
        return (cls.open, cls.read_only)

    @classmethod
    @Choices.ToIDs
    def editable(cls):
        return (cls.open,)


class PublishedManager(db.Manager):
    def get_query_set(self):
        # get the original query set
        query_set = super(PublishedManager, self).get_query_set()
        # leave rows with viewable status
        query_set = query_set.filter(publication_status__in =
                                     PublicationStatus.viewable())
        # leave rows with publication date in the past (or none at all)
        query_set = query_set.filter(db.Q(visible_from = None) |
                                     db.Q(visible_from__lte = datetime.now()))
        # leave rows with expiry date in the future (or none at all)
        query_set = query_set.filter(db.Q(visible_to = None) |
                                     db.Q(visible_to__gte = datetime.now()))
        return query_set


class Publishable(db.Model):
    publication_status = db.IntegerField(verbose_name=_("publication status"),
            choices=PublicationStatus(), default=PublicationStatus.draft.id)
    visible_from = db.DateTimeField(verbose_name=_("Visible on site from"),
            blank=True, null=True)
    visible_to = db.DateTimeField(verbose_name=_("Visible on site to"),
            blank=True, null=True)
    include_on_frontpage = db.BooleanField(verbose_name=_("Visible on frontpage?"),
        default=True, help_text=_("If unchecked, the entry is not available "
            "in the recent list nor the archive, but it is still listed "
            "in the tag listings and the sitemap."), db_index=True)
    _sort_date = db.DateTimeField(blank=True, null=True, default=None,
        editable=False)

    admin_objects = db.Manager()
    objects = PublishedManager()

    class Meta:
        abstract = True

    @property
    def is_published(self):
        try:
            self.__class__.objects.get(pk=self.pk)
            return True
        except self.DoesNotExist:
            return False

    def get_sort_date(self):
        return self._sort_date

    def get_modification_date(self):
        if not self.modified or not self.created or \
            (self.modified - self.created).seconds < 60 or \
            (self.visible_from and self.visible_from > self.modified):
            return None
        return self.modified

    def save(self, *args, **kwargs):
        if self.visible_from:
            self._sort_date = self.visible_from
        else:
            self._sort_date = self.created
        super(Publishable, self).save(*args, **kwargs)


class Commentable(db.Model):
    comment_status = db.IntegerField(verbose_name=_("comment status"),
            choices=CommentStatus(), default=CommentStatus.open.id,
            help_text=_("If the comments are open, logged in users can add "
                "new comments to the article. In read-only mode, existing "
                "comments are still visible. When comments are closed, "
                "they're not visible and adding new is not possible."))

    @property
    def are_comments_visible(self):
        return self.comment_status in CommentStatus.viewable()

    @property
    def are_comments_editable(self):
        return self.comment_status in CommentStatus.editable()

    class Meta:
        abstract = True


class Entry(TimeTrackable, EditorTrackable, Publishable, Commentable,
    DisplayCounter):
    source_name = db.CharField(verbose_name=_("Source (name)"), max_length=100,
        blank=True, default="")
    source_url = db.URLField(verbose_name=_("Source (URL)"), blank=True,
        null=True, default=None)

    class Meta:
        ordering = ('-_sort_date',)
        verbose_name = _("entry")
        verbose_name_plural = _("entries")

    @db.permalink
    def get_absolute_url(self):
        return ('langapl.publisher.views.show', [self.id, 'redirect'])

    def __unicode__(self):
        english_content = self.content_set.filter(language=Language.en.id)
        if english_content.exists():
            return unicode(english_content[0])

    def get_editor_from_request(self, request):
        return request.user.get_profile()


class VisibleContentManager(db.Manager):
    def get_query_set(self):
        # get the original query set
        query_set = super(VisibleContentManager, self).get_query_set()
        # leave rows with viewable status
        query_set = query_set.filter(entry__publication_status__in =
                                     PublicationStatus.viewable())
        # leave rows with publication date in the past (or none at all)
        query_set = query_set.filter(db.Q(entry__visible_from = None) |
                                     db.Q(entry__visible_from__lte =
                                         datetime.now()))
        # leave rows with expiry date in the future (or none at all)
        query_set = query_set.filter(db.Q(entry__visible_to = None) |
                                     db.Q(entry__visible_to__gte =
                                         datetime.now()))
        return query_set


class TextAndHTML(TimeTrackable):
    summary_raw = db.TextField(verbose_name=_("short summary"))
    content_raw = db.TextField(verbose_name=_("content"))
    summary_html = db.TextField(editable=False, default="")
    content_html = db.TextField(editable=False, default="")
    summary_notags = db.TextField(editable=False, default="")
    content_notags = db.TextField(editable=False, default="")

    insignificant_fields = TimeTrackable.insignificant_fields | {
        'summary_html', 'content_html', 'summary_notags', 'content_notags',
    }

    class Meta:
        abstract = True

    def restructuredtext(self, value):
        parts = publish_parts(
            source=smart_str(value), writer_name="html4css1",
            settings_overrides=RESTRUCTUREDTEXT_FILTER_SETTINGS,
        )
        return mark_safe(force_unicode(parts["fragment"]))

    def save(self, *args, **kwargs):
        self.summary_html = self.restructuredtext(self.summary_raw)
        self.summary_notags = TAG_REGEX.sub('', self.summary_html)
        self.content_html = self.restructuredtext(self.content_raw)
        self.content_notags = TAG_REGEX.sub('', self.content_html)
        super(TextAndHTML, self).save(*args, **kwargs)


class Content(Titled, Slugged, TextAndHTML, EditorTrackable,
    Taggable, Localized, DisplayCounter):
    entry = db.ForeignKey(Entry, verbose_name=_("entry"))

    admin_objects = db.Manager()
    objects = VisibleContentManager()

    class Meta:
        ordering = ('-entry___sort_date', 'language')
        unique_together = ("entry", "language")
        verbose_name = _("content")
        verbose_name_plural = _("contents")

    @db.permalink
    def get_absolute_url(self):
        return ('langapl.publisher.views.show', [self.entry_id, self.slug])

    def save(self, *args, **kwargs):
        if not self.created_by:
            self.created_by = self.entry.created_by
        super(Content, self).save(*args, **kwargs)
        try:
            ping_google()
        except Exception:
            pass

    def get_editor_from_request(self, request):
        return request.user.get_profile()

    def get_sort_date(self):
        return self.entry.get_sort_date()

    def get_modification_date(self):
        if not self.modified or not self.created or \
            (self.modified - self.created).seconds < 60 or \
            (self.entry.visible_from and self.entry.visible_from >
                self.modified):
            return None
        return self.modified

    def similar(self, how_many):
        invalidator = Content.objects.aggregate(db.Max('modified'))[
            'modified__max']
        cache_key = 'article_{}_similar_{}'.format(self.id, how_many)
        similar_set = cache.get(cache_key, invalidator=invalidator)
        if not similar_set:
            similar_set = self.similar_objects()[:how_many]
            if similar_set:
                cache.set(cache_key, similar_set,
                    invalidator=invalidator, timeout=48*3600)
        return similar_set

    def admin_base_url(self):
        return "admin/{}/{}".format(self._meta.app_label, self._meta.module_name)

    @property
    def publication_status(self):
        return self.entry.publication_status

    def get_publication_status_display(self):
        return self.entry.get_publication_status_display()
    get_publication_status_display.short_description = _("publication status")

    def visible_from(self):
        return self.entry.visible_from
    visible_from.short_description = _("Visible on site from")

    def visible_to(self):
        return self.entry.visible_to
    visible_to.short_description = _("Visible on site to")


class PrivateNote(Titled, Slugged, TextAndHTML, EditorTrackable, Publishable,
    Commentable, Localized, DisplayCounter):
    private_key = db.CharField(verbose_name=_("private key"), max_length=32)

    class Meta:
        ordering = ('-_sort_date',)
        verbose_name = _("private note")
        verbose_name_plural = _("private notes")

    @db.permalink
    def get_absolute_url(self):
        return ('langapl.publisher.views.private', [self.slug])

    def get_editor_from_request(self, request):
        return request.user.get_profile()

    def bump(self, address, failed_key=""):
        success = not bool(failed_key)
        PrivateAuthLog.objects.create(
                address=address, success=success, note=self,
                failed_key=failed_key)
        if success:
            status = "accessed"
        else:
            status = "FAILED to access"
        subject = "{} {} private note {}".format(address, status,
                self.get_absolute_url())
        message = """
        <h1>{}</h1>
        <p>
        Link to details: <a href="http://lukasz.langa.pl/admin/publisher/privatenote/{}/">
        http://lukasz.langa.pl/admin/publisher/privatenote/{}/</a>
        </p>
        """.format(subject, self.id, self.id)
        mail_admins(subject, message, fail_silently=True, html_message=message)
        if success:
            return super(PrivateNote, self).bump(address)


class PrivateAuthLog(TimeTrackable):
    note = db.ForeignKey(PrivateNote, verbose_name=_("private note"))
    success = db.BooleanField(verbose_name=_("success"))
    address = db.IPAddressField(verbose_name=_("IP address"))
    failed_key = db.CharField(verbose_name=_("failed key"), max_length=32,
        blank=True, default="")

    class Meta:
        ordering = ('-modified',)
        verbose_name = _("private auth log")
        verbose_name_plural = _("private auth logs")

    def __unicode__(self):
        return ''
