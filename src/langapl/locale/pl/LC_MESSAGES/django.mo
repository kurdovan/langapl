��    =        S   �      8  �   9  �   �  *   �      �     �                    '     *     1     7     I     V     f     l       �   �  �   Y     �  
   �     �     �     	     	  	   &	  	   0	     :	     I	     V	     s	     �	     �	     �	     �	  1   �	  9   �	     "
     8
     M
     `
     d
     s
     {
     �
     �
     �
     �
     �
     �
  )   �
               $     2     :     C     V     d     j  �  r  �     �   �  '   �  "   �          %     .     7     K  
   N     Y     h     }     �     �     �     �     �  �   �     ~     �     �     �      �     �     �     �               '     F     V     h     p     t  ;   �  X   �     (     G     ^     u     z     �     �  
   �     �     �     �     �     �  .        3     B     Q     `     g     o          �     �        #                 ,   .       *          2      &             :   )      (         !           /              =       3              8          1      %          "   $          -                   9   ;          6          '          +      	   <                    4   0           
   5   7       
Maybe it doesn't exist. Maybe it never existed. Maybe it existed in an alternative version of the site. Maybe it requires extraordinary privileges. Or maybe you just made the URL up?
 
Response status codes beginning with the digit "5" indicate cases in which the server is aware that it has encountered an error or is otherwise incapable of performing the request. 
 404: the page you requested is unavailable 500: Something horrible happened An error occured Archive Article Back to basics. CV Closed Draft Entries tagged as Entry locked Entry tagged as Error For your eyes only Home If the comments are open, logged in users can add new comments to the article. In read-only mode, existing comments are still visible. When comments are closed, they're not visible and adding new is not possible. If unchecked, the entry is not available in the recent list nor the archive, but it is still listed in the tag listings and the sitemap. Music Needs Edit Open Open source Page not found. Powered by Python Published Read-only Recent entries Show on site Something horrible happened. Source (URL) Source (name) Submit Tag The key provided is invalid. The page you requested is apparently unavailable. The server failed to fulfill an apparently valid request. Visible on frontpage? Visible on site from Visible on site to and comment status content contents empty entry entries entry last updated lukasz.langa.pl Admin lukasz.langa.pl: Recent Entries lukasz.langa.pl: Recent entries tagged as private key private note private notes profile profiles publication status short summary title updated Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2012-04-04 13:44+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2)
 
Może nie istnieje. Może nigdy nie istniała. Może istniała w alternatywnej odsłonie tego serwisu. Może wymaga nadzwyczajnych uprawnień, żeby móc ją oglądać. A może po prostu zmyśliłeś/-aś URL?
 
Kody statusu HTTP zaczynające się cyfrą "5" oznaczają przypadki, w których serwer jest świadomy, że natrafił na wewnętrzny błąd lub inny błąd, który uniemożliwia mu wygenerowanie odpowiedzi na zapytanie.
 404: żądana strona nie jest dostępna 500: Zdarzyło się coś okropnego Wystąpił błąd Archiwum Artykuł Powrót do podstaw. CV Zamknięte Wersja robocza Wpisy otagowane jako Wpis pod kluczem Wpis otagowany jako Błąd Tylko dla Twoich oczu Strona główna Kiedy komentarze są otwarte, zalogowani użytkownicy mogą dodawać nowe komentarze. W trybie tylko do odczytu, istniejące komentarze są widoczne, ale dodawanie nowych nie jest możliwe. Kiedy komentarze są zamknięte, nie są widoczne na stronie i dodawanie nowych nie jest możliwe. Jeżeli niezaznaczone, wpis nie jest dostępny w liście ostatnich ani w archiwum, ale nadal znajduje się listingu tagów i w mapie witryny. Muzyka Potrzebuje edycji Otwarte Open source Strona nie została odnaleziona. Serwis stworzony w Pythonie Opublikowane Tylko do odczytu Ostatnie wpisy Pokaż na stronie Coś okropnego się zdarzyło. Żródło (URL) Źródło (nazwa) Wyślij Tag Podany klucz jest niepoprawny. Strona, o którą prosisz, jest najwyraźniej niedostępna. Serwer nie był w stanie wygenerować odpowiedzi na najwyraźniej prawidłowe zapytanie. Widoczne na stronie głównej? Widoczne na stronie od Widoczne na stronie do oraz stan komentarzy treść treści pusty wpis wpisy wpis ostatnia aktualizacja Administracja lukasz.langa.pl lukasz.langa.pl: Ostatnie wpisy lukasz.langa.pl: Ostatnie wpisy otagowane jako klucz prywatny prywatna notka prywatne notki profil profile stan publikacji podsumowanie tytuł ostatnia aktualizacja 